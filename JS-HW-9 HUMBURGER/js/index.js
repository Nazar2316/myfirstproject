// * @constructor
// * @param size        Размер
// * @param stuffing    Начинка
// * @throws {HamburgerException}  При неправильном использовании

function Hamburger(size, stuffing){

  this.size = size;
  this.stuffing = stuffing;
  this.topping = [];
}

                                  /* Размеры, виды начинок и добавок */
// SIZE
Hamburger.SIZE_SMALL = {
  price: 50,
  calories: 20
};
Hamburger.SIZE_LARGE = {
  price: 100,
  calories: 40
};

// STUFFING
Hamburger.STUFFING_CHEESE = {
  price: 10,
  calories: 21
};
Hamburger.STUFFING_SALAD = {
  price: 20,
  calories: 5
};
Hamburger.STUFFING_POTATO = {
  price: 15,
  calories: 10
};

// TOPPING
Hamburger.TOPPING_MAYO = {
  price: 20,
  calories: 5
};
Hamburger.TOPPING_SPICE = {
  price: 15,
  calories: 0
};


                                   //   ДОБАВИТЬ - ДОБАВКУ к гамбургеру

Hamburger.prototype.addTopping = function (topping) {
  //????????????????
  // /**
  // Можно добавить НЕСКОЛЬКО добавок при условии, что они разные.
  // *
  // * @param topping     Тип добавки
  // * @throws {HamburgerException}  При неправильном использовании
  // */
  // try {
  //   // ??????????????????
  //   if (topping == TOPPING_MAYO && topping == TOPPING_SPICE) {
  //     document.write(TOPPING_MAYO + TOPPING_SPICE);
  //   } else if (topping == TOPPING_SPICE) {
  //     document.write(TOPPING_SPICE);
  //   }else if (topping == TOPPING_MAYO){
  //     document.write(TOPPING_MAYO);
  //   }

  // } catch (e) {
  //    if (!topping){
  //       throw new SyntaxError("Ошибка в данных, не был добавлен обьязательный парметр");
  //    }else{
  //       console.log(e.message);
  //       console.log(e.name);
  //       console.log(e.lineNumber);
  //       console.log(e.columnNumber);
  //       console.log(e.stack);
  //    }
  // }
  if (!this.topping.includes(topping)) {
    return this.topping.push(topping);
  }
};

                             //    УБРАТЬ - ДОБАВКУ ПРИ УСЛОВИИ ЧТО ОНА РАНЕЕ БЫЛА ДОБАВЛЕНА  ????????????????
            
Hamburger.prototype.removeTopping = function (topping){
  // /**
  //  * Получить список добавок.
  //  *
  //  * @return {Array} Массив добавленных добавок, содержит константы
  //  *                 Hamburger.TOPPING_*
  //  */
  // if(topping == TOPPING_MAYO && topping == TOPPING_MAYO){
    
  // }else if(topping == TOPPING_SPICE && topping == TOPPING_SPICE){
    
  // }
  delete this.topping[topping]; //?????????
  return this.topping;

}

                                    // ПОЛУЧИТЬ - СПИСОК ДОБАВОК-

Hamburger.prototype.getTopping = function () {       

  return this.topping;
    
}

                                 //    УЗНАТЬ - РАЗМЕР - гамбургера

Hamburger.prototype.getSize = function (){
    return this.size;
}

                                 //    УЗНАТЬ - НАЧИНКУ - гамбургера

Hamburger.prototype.getStuffing = function (){
    return this.stuffing;
}

            //   УЗНАТЬ - ЦЕНУ - гамбургера 

//  @return {Number} Цена в тугриках

Hamburger.prototype.calculatePrice = function (){

  const SIZE = this.getSize();
  let price = SIZE["price"];
  
  let topping = this.getTopping();
  topping.forEach(function (item) {
    price += item["price"];
  });

  let stuffing = this.getStuffing();
  price += stuffing['price' ]

  return price;

}
               
            //    УЗНАТЬ - КАЛОРИЙНОСТЬ
            
//  @return {Number} Калорийность в калориях

Hamburger.prototype.calculateCalories = function () {

  const SIZE = this.getSize();
  let calories = SIZE["calories"];

  let topping = this.getTopping();
  topping.forEach(function (item) {
    calories += item["calories"];
  });

  let stuffing = this.getStuffing();
  calories += stuffing["calories"];

  return calories;
  
}

// маленький гамбургер с начинкой из сыра
               let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
               // добавка из майонеза
               hamburger.addTopping(Hamburger.TOPPING_MAYO);
               // спросим сколько там калорий
               console.log("Calories: %f", hamburger.calculateCalories());
               // сколько стоит
               console.log("Price: %f", hamburger.calculatePrice());
               // я тут передумал и решил добавить еще приправу
               hamburger.addTopping(Hamburger.TOPPING_SPICE);
               // А сколько теперь стоит? 
               console.log("Price with sauce: %f", hamburger.calculatePrice());

               // Проверить, большой ли гамбургер? 
               console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false

               // Убрать добавку
               hamburger.removeTopping(Hamburger.TOPPING_SPICE);
               console.log("Have %d toppings", hamburger.getTopping().length); // 1

               
              // -------------------------- FIX REMOVE Not working------------------