//                                            1 ЗАДАНИЕ


// Конструктор
// function Human(age, name){
//     this.age = age;
//     this.name = name;
// }

// let person_1 = new Human(10, " Уася"),
//     person_2 = new Human(30, " Петя"),
//     person_3 = new Human(20, " Шарик");

// // Массив с обьектами
// let arr = [person_1, person_2, person_3];

// // Метод для сортировки
// function sortAge(arr){
//   arr.sort((a, b) => a.age > b.age ? 1 : -1);
// }
// // Вызов метода
// sortAge(arr);

// console.log(arr);



//                                             2 ЗАДАНИЕ


// Конструктор

function Human(name, position, salary) {
  
  this.name = name;
  this.position = position;
  this.annualSalary = salary;
  // кл-во месяцев в году
  this.year = 12;

  // создаём метод greet()
  this.greet = function(){
    document.write(`Hello my name is ${this.name}, i'am working as a ${this.position}:<br>`);
  }
}

// метод  который будет высчитывать месячную зарплату 
Human.prototype.monthlySalary = function monthlySalary() {
  document.write(`${this.name}  is a ${this.gender} : <br> His monthly salary - ${this.annualSalary / this.year} $ <br><br>`);
};

// Обьекты
person_1 = new Human("Steffan", " Meneger ", 60000),
person_2 = new Human("Marko", " CEO ", 111000),
person_3 = new Human("Alex", " Assistant ", 42000);
    
// Дабавляем свойство (gender)
person_1.gender = "male";
person_2.gender = "male";
person_3.gender = "male";

 
// Вызови функц.
person_1.greet();
person_1.monthlySalary();

person_2.greet();
person_2.monthlySalary();

person_3.greet();
person_3.monthlySalary();