function Validator() {
  //для проверки mail
  this.isEmail = function (mail) {
    let emailPattern = /^\w+@[a-zA-Z]+\.[a-zA-Z]{2,3}/gim;
    return emailPattern.test(mail);
  };
  //для проверки домена
  this.isDomain = function (domain) {
    let domainPattern = /^(?:(?:https?|ftp|telnet):\/\/(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:com|net|org|mil|edu|arpa|ru|gov|biz|info|aero|inc|name|[a-z]{2})|(?! 0)(?:(?! 0[^.]|255)[ 0-9]{1,3}\.){3}(?! 0|255)[ 0-9]{1,3})(?:\/[a-zа-я0-9.,_@%&?+=\~\/-]*)?(?:#[^ \'\"&<>]*)?$/gi;
    return domainPattern.test(domain);
  };

  //для проверки date
  this.isDate = function (date) {
    //для проверки даты
    let datePattern = /\d{1,2}\.\d{1,2}\.\d{4}/g;
    return datePattern.test(date);
  };

  //для проверки phone
  this.isPhone = function (tel) {
    //для проверки тел.
    let phonePattern = /^\+375 \(\d{2}\) \d{3}-\d{2}-\d{2}/;
    return phonePattern.test(tel);
  };
}

let validator = new Validator();

console.log(validator.isEmail("phphtml@mail.ru"));
console.log(validator.isDomain("phphtml.net"));
console.log(validator.isDate("12.05.2020"));
console.log(validator.isPhone("+375 (29) 817-68-92"));
