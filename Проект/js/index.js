window.addEventListener("DOMContentLoaded", () => {
  // ------ToggleSwitch----- ((До))-----                -----ПЕРВЫЙ ВАРИАНТ ------

  const monthlyButton = document.querySelector("#monthly");
  const annualyButton = document.querySelector("#annualy");

  const toggleSwitch = document.querySelector(".toggle");

  const monthlyItem = document.querySelector(".monthlyItem");
  const annualyItem = document.querySelector(".annualyItem");

  // const removeClass = (item) => {
  //   item.classList.remove("_active");
  // };
  // const addClass = (item) => {
  //   item.classList.add("_active");
  // };

  // toggleSwitch.addEventListener("click", () => {
  //   if (annualyButton.checked) {
  //     removeClass(monthlyItem);
  //     addClass(annualyItem);
  //   } else if (monthlyButton.checked) {
  //     removeClass(annualyItem);
  //     addClass(monthlyItem);
  //   }
  // });

  // if (monthlyButton.checked) {
  //   addClass(monthlyItem);
  // } else if (annualyButton.checked) {
  //   removeClass(monthlyItem);
  //   addClass(annualyItem);
  // }

  // ------ToggleSwitch----- ((После)) ------------ ВТОРОЙ ВАРИАНТ-------------Упрощённый код

  toggleSwitch.addEventListener("click", () => {
    if (monthlyButton.checked) {
      monthlyItem.classList.toggle("_active");
    } else if (annualyButton.checked) {
      annualyItem.classList.toggle("_active");
    }
  });























  // -----ACCORDEON-------

  let acc = document.querySelectorAll(".accordion-item__button");

  for (let i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      this.classList.toggle("active-accordion");
      let panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }

  // ------SLIDER PERSON-------

  const nextSlide = document.querySelector(".next");
  const previousSlide = document.querySelector(".previous");

  /* Увеличиваем индекс на 1 — показываем следующий слайд*/
  nextSlide.addEventListener("click", () => {
    showSlides((slideIndex += 1));
  });

  /* Уменьшает индекс на 1 — показываем предыдущий слайд*/
  previousSlide.addEventListener("click", () => {
    showSlides((slideIndex -= 1));
  });

  /* Устанавливаем индекс слайда по умолчанию */
  let slideIndex = 1;
  showSlides(slideIndex);

  /* Функция перелистывания */
  function showSlides(n) {
    let slides = document.querySelectorAll(".item");
    if (n > slides.length) {
      slideIndex = 1;
    }
    if (n < 1) {
      slideIndex = slides.length;
    }
    /* Проходим по каждому слайду в цикле for */
    for (let slide of slides) {
      slide.style.display = "none";
    }
    slides[slideIndex - 1].style.display = "inline-flex";
  }

  // -------BURGER MENU------
  const nav = document.getElementById("navUl");
  const burger = document.querySelector(".header__burger");
  const body = document.querySelector("body");
  let menuOpen = false;

  burger.addEventListener("click", () => {
    // Show Menu
    nav.classList.toggle("menu-list--active");
    // Disable Scroll
    body.classList.toggle("modal--open");

    // Burger Animation
    if (!menuOpen) {
      burger.classList.add("open");
      menuOpen = true;
    } else {
      burger.classList.remove("open");
      menuOpen = false;
    }
  });
});
// Toggle Class зверху редакт (укоротити код)