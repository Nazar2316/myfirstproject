// обьект ( doc )
const doc = {

    title  : "Сериал Викинги",
    content   : "топ - Сериали",
    footer : "History",
    data   : "2010 - 2015",

    // метод для отображенияелементов обьекта
    show: function(){
        document.write("Заголовок: " + this.title + "<br>"+ "Тело: " + this.content + "<br>"+ "Футер: " + this.footer + "<br>"+ "Дата: " + this.data + "<br>" +"<hr>")
    },

    // создаю вложеный обьект ( app )
        app : {
            title2  : "Ридик",
            
            content2   : "фильм" ,

            footer2 : "Universal Pictures" ,

            data2   : "2013 год" ,

            // метод для отображенияелементов обьекта
            show: function(){
                document.write("Заголовок фильма: " + this.title2 + "<br>"+ "Тело фильма: " + this.content2 + "<br>"+ "Футер фильма: " + this.footer2 + "<br>"+ "Дата фильма: " + this.data2);
            },
        }     
}
// вызовы функций
doc.show();
doc.app.show();




// // Второй способ создание оьектов и добавление нужных значений и елементов


// // создаю обьект ( doc )
// const doc = new Object();

// // создаю вложенный обьект ( app )
// doc.app = new Object();

// doc.title = function(){
//     document.write('Заголовок' + '<br>');
// }

// doc.content = function(){
//     document.write('Тело' + '<br>');
// }
// doc.footer = function(){
//     document.write('Футер' + '<br>');
// }
// doc.data = function(){
//     document.write('Дата' + '<br>' + "<hr>");
// }

// // функции вложенного обьекта

// doc.app.title = function(){
//     document.write('Заголовок' + '<br>');
// }

// doc.app.content = function(){
//     document.write('Тело' + '<br>');
// }
// doc.app.footer = function(){
//     document.write('Футер' + '<br>');
// }
// doc.app.data = function(){
//     document.write('Дата' + '<br>');
// }
// // Вызываем функии обьекта ( doc )
// doc.title();
// doc.content();
// doc.footer();
// doc.data();
// // Вызываем функии вложенного обьекта (app)
// doc.app.title();
// doc.app.content();
// doc.app.footer();
// doc.app.data();

