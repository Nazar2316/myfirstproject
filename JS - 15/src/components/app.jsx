import React from "react";
import Header from "./header/header";
import Section from "./section/section"
import Footer from "./footer/footer"

const App = ()=>{
    return(
        <>
            <Header/>
            <Section/>
            <Footer/>
        </>
    )
}

export default App;