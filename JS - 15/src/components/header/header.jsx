import React from "react";
import image from "./img/img.png";
import  Ul from "./ul.jsx";


const Header = ()=>{
    return(
        
        <header>
            <Ul/>
            <img src={image} alt="img"/>
        </header>
    )
}

export default Header;