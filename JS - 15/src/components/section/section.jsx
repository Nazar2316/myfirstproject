import React from "react";
import P from "./p"
import H1 from "./h1"
import Button from "./button"

const Section = ()=>{
    return(
        <section className="text">
            <H1/>
            <P/>
            <Button/>
        </section>
    )
}

export default Section;