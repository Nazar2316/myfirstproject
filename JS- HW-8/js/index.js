/*
ПРи загрузке страницы: показать на ней BTN с текстом ("Нарисовать круг").
Данная кнопка должна являтся единственным контентом в теле HTML докум. , весь остальной контент должен быть создан и добавлен на страницу 
с помощью JS/
При нажатии на кнопку ("Нарисовать круг") показывать одно поле ввода - диаметр круга.
При нажатии на кнопку ("Нарисовать") создать на странице  100кругов (10*10) случайного цвета.

ПРи клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняется, тоесть все остальные круги сдвигаются влево.*/

// ПРи загрузке страницы:
window.addEventListener("DOMContentLoaded", () =>{
    const btn = document.querySelector("input");
    

    // При нажатии на кнопку, показать одно поле ввода - диаметр круга.
    btn.addEventListener("click", () =>{

        // получаем Диаметр круга от пользоваетля
        let size = +prompt("Введите диаметр круга") + "px";

        // Функция для рандомных цветов
        colorRandom = () =>{
            let random = Math.floor(Math.random() * (1000)),
            color = "#" + random.toString();
            return color;
        }

        // функция для рисования круга
        const drawCircle = () => {   

            // Создаём (div)
            let circle = document.createElement("div");

            // при нажатии на него он будет удалятся
            circle.addEventListener("click", () =>{
                circle.parentNode.removeChild(circle);
            })
            
            // Добавляем его в (body)
            document.body.append(circle);

            // Устанавливаем параметры дабы сделать из ДИВА - КРУГ
            circle.style.width = size;
            circle.style.height = size;
            circle.style.borderRadius = "50%";
            circle.style.border = "3px solid";
            circle.style.display = "inline-block"
            circle.style.borderColor = colorRandom();
            
        };

        // Цикл для рисовки кругов 10*10
        for (let i = 0; i <= 10; i++) {

            for (let j = 0; j < 10; j++) {
              // вызываем функц.
              drawCircle();
            }
            // После десяти кругов опускаемся на строку ниже
            document.write('<br>');
        }
    })
})

